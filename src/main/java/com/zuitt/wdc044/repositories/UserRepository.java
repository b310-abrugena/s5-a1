// s02 Activity
package com.zuitt.wdc044.repositories;

import com.zuitt.wdc044.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Object> {
//    This will be used in creating a JWT(JwtToken class)
    User findByUsername(String username);
}

