package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface PostService {
    void createPost(String stringToken, Post post);
    // Now that we are generating JWT, ownership of a blog post will be retrieved from the JWT's payload.

    Iterable<Post> getPosts();

    // s05 activity code
    Iterable<Post> userPosts(String stringToken);

//      Long id as the post id
//      String stringToken as user credentials
//      Post post as request body
    ResponseEntity updatePost(Long id, String stringToken, Post post);
    ResponseEntity deletePost(Long id, String stringToken);


}
